require 'csv'
require 'open-uri'

games = CSV.parse(open("http://nopaystation.com/tsv/PSV_GAMES.tsv"), { :col_sep => "\t", :headers => true })
demos = CSV.parse(open("http://nopaystation.com/tsv/PSV_DEMOS.tsv"), { :col_sep => "\t", :headers => true })


result = ""
Dir["PCS*/*.ppk"].each do |game|
	game_title_id = game.split("/")[0]
  begin
    game_name = games.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
  rescue
    game_name = demos.find {|i| i["Title ID"] == game.split("/")[0] }["Name"]
  end
	result += "#{game}=#{game_name}\n"
end

puts result
